import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import asciiUtils.AsciiPackage;
import asciiUtils.Result;
import asciiUtils.Result.ColorType;


public class AsciiServerThread implements Runnable{

	public ObjectOutputStream toClient = null;
	public ObjectInputStream fromClient = null;
	AsciiPackage before;
	Socket clientSocket;
	


	public AsciiServerThread(Socket s, ObjectOutputStream o, ObjectInputStream i){
		clientSocket = s;
		toClient = o;
		fromClient = i;
	}
	
	@Override
	public void run(){
	
		try {
			Object obj = fromClient.readObject();
			System.out.println("object read");
			if(obj instanceof AsciiPackage){
				before = (AsciiPackage)obj;
		        System.out.println("isChanged before: " + before.result.isChanged);
				before.computeData();
				toClient.writeObject(before);
		        System.out.println("isChanged after: " + before.result.isChanged);
			}else{
				System.out.println("Before is null");
			}
		}catch (EOFException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}finally{
			try {
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	
}
