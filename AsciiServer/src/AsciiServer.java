import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import asciiUtils.AsciiPackage;
import asciiUtils.Result;


public class AsciiServer implements Runnable{
	
	public static final int PORT_NUM = 9999;
	public ServerSocket serverSocket;
	Socket clientSocket;
	ObjectOutputStream toClient;
	ObjectInputStream fromClient;

	public AsciiServer(){
		
		try{
			serverSocket = new ServerSocket(PORT_NUM);
			System.out.println("Server was successful");
			
		}catch (IOException e){
			e.printStackTrace();
		}
			
	}
	
	@Override
	public void run(){
		while(true){
			System.out.println("new thread started");
			try {
				clientSocket = serverSocket.accept();
				toClient = new ObjectOutputStream((clientSocket.getOutputStream()));
				toClient.flush();
				fromClient = new ObjectInputStream((clientSocket.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}
			Thread ser = new Thread(new AsciiServerThread(clientSocket, toClient, fromClient));
			ser.start();
//			try {
//				ser.join();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
			System.out.println("Client was accepted");
		}
	}
		
}
	
	


