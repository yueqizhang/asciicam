package asciiUtils;

import java.io.Serializable;


public class Result implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5528611592608735338L;
	public final static float ANSI_COLOR_RATIO = 7.0f/8;
	
	public int rows;
	public int columns;
	public ColorType colorType;
	public String[] pixelChars;
	public String debugInfo;
	public int[] asciiIndexes;
	public int[] asciiColors;
	public boolean isChanged;

	public Result(){
		isChanged = false;

	}
	
	public static enum ColorType {
    	// all same color
        NONE(" .:oO8@"),
        
        // primary colors and combinations (red/green/blue/cyan/magenta/yellow/white)
        ANSI_COLOR(" .:oO8@"),
        
        // all colors
        FULL_COLOR("O8@");
        
        String[] pixelChars;
        
        private ColorType(String pixelCharString) {
        	this.pixelChars = toPixelCharArray(pixelCharString);
        }
        
        public String[] getDefaultPixelChars() {
        	return pixelChars;
        }
    }
	
	private static String[] toPixelCharArray(String str) {
        if (str==null || str.length()==0) return null;
        String[] charArray = new String[str.length()];
        for(int i=0; i<str.length(); i++) {
            charArray[i] = str.substring(i, i+1);
        }
        return charArray;
    }
	
	 public ColorType getColorType() {
         return colorType;
     }
     
     public String stringAtRowColumn(int row, int col) {
         return pixelChars[asciiIndexes[row*columns + col]];
     }
     
     public int colorAtRowColumn(int row, int col) {
         if (colorType==ColorType.NONE) return 0xffffffff;
         return asciiColors[row*columns + col];
     }
     
     public float brightnessRatioAtRowColumn(int row, int col) {
         return 1.0f*asciiIndexes[row*columns + col] / pixelChars.length;
     }
     
     public String getDebugInfo() {
         return debugInfo;
     }
     
     public Result copy() {
         Result rcopy = new Result();
         rcopy.rows = this.rows;
         rcopy.columns = this.columns;
         rcopy.colorType = this.colorType;
         if (pixelChars!=null) rcopy.pixelChars = pixelChars.clone();
         if (asciiIndexes!=null) rcopy.asciiIndexes = asciiIndexes.clone();
         if (asciiColors!=null) rcopy.asciiColors = asciiColors.clone();
         return rcopy;
     }
	
	
	
	
}
