package asciiUtils;


import java.io.Serializable;

import asciiUtils.Result.ColorType;

/**
 * Packages all the necessary variables into an object,
 * and passes it to the server.
 *
 */
public class AsciiPackage implements Serializable{
	
	
    static boolean nativeCodeAvailable = false;
    
    public native void getAsciiValuesWithColorNative(byte[] jdata, int imageWidth, int imageHeight, 
            int asciiRows, int asciiCols, int numAsciiChars, boolean ansiColor, 
            int[] jasciiOutput, int[] jcolorOutput, int startRow, int endRow);

    public native void getAsciiValuesBWNative(byte[] jdata, int imageWidth, int imageHeight, 
            int asciiRows, int asciiCols, int numAsciiChars, int[] jasciiOutput, int startRow, int endRow);

//    static {
//        try {
//            System.loadLibrary("asciiart");
//            nativeCodeAvailable = true;
//        }
//        catch(Throwable ignored) {}
//    }
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2394268503555716312L;
	public Result result;
	public byte[] data;
	public int imageHeight;
	public int imageWidth;
	public int startRow;
	public int endRow;
	
	public AsciiPackage(){
		result = null;
		data = null;
		imageHeight = 0;
		imageWidth = 0;
		startRow = 0;
		endRow = 0;
	}
	public AsciiPackage(Result r, byte[] d, int w, int h, int s, int e){
		result = r;
		data = d;
		imageHeight = h;
		imageWidth = w;
		startRow = s;
		endRow = e;
	}
	

	public void computeData(){
//		byte[] data = before.data;
//		int imageWidth = before.imageWidth;
//		int imageHeight = before.imageHeight;
//		Result result = before.result;
//		int asciiRows = result.rows;
//		int asciiCols = result.columns;
//		String[] pixelChars = result.pixelChars;
//		Result.ColorType colorType = result.colorType;
//		int startRow = before.startRow;
//		int endRow = before.endRow;
//		int[] asciiIndexes = result.asciiIndexes;
//		int[] asciiColors = result.asciiColors;
//		AsciiPackage temp = new AsciiPackage();
		result.isChanged = true;
		if(result.colorType!=ColorType.NONE){
			if (nativeCodeAvailable) {
	          getAsciiValuesWithColorNative(data, imageWidth, imageHeight, result.rows, result.columns, 
	                  result.pixelChars.length, result.colorType==ColorType.ANSI_COLOR, result.asciiIndexes, result.asciiColors,
	                  startRow, endRow);
			}else{
			
			final int MAX_COLOR_VAL = (1 << 18) - 1;
		        int asciiIndex = startRow * result.columns;
		        for(int r=startRow; r<endRow; r++) {
		            // compute grid of data pixels whose brightness and colors to average
		            int ymin = imageHeight * r / result.rows;
		            int ymax = imageHeight * (r+1) / result.rows;
		            for(int c=0; c<result.columns; c++) {
		                int xmin = imageWidth * c / result.columns;
		                int xmax = imageWidth * (c+1) / result.columns;
		                
		                int totalBright = 0;
		                int totalRed=0, totalGreen=0, totalBlue=0;
		                int samples = 0;
		                for(int y=ymin; y<ymax; y++) {
		                    int rowoffset = imageWidth * y;
		                    // UV data is only stored for every other row and column, so there are 1/4 as many (U,V) byte
		                    // pairs as there are pixels (and 1/2 as many total UV bytes).
		                    int uvoffset = imageWidth * imageHeight + (imageWidth * (y / 2));
		                    for(int x=xmin; x<xmax; x++) {
		                        samples++;
		                        int bright = 0xff & data[rowoffset+x];
		                        totalBright += bright;
		                        // YUV to RGB conversion, produces 18-bit RGB components
		                        // adapted from http://stackoverflow.com/questions/8399411/how-to-retrieve-rgb-value-for-each-color-apart-from-one-dimensional-integer-rgb
		                        int yy = bright - 16;
		                        if (yy < 0) yy = 0;
		                        
		                        int uvindex = uvoffset + (x & ~1); // 0, 0, 2, 2, 4, 4...
		                        int v = (0xff & data[uvindex]) - 128;
		                        int u = (0xff & data[uvindex + 1]) - 128;
		                        int y1192 = 1192 * yy;
		                        int red = (y1192 + 1634 * v);  
		                        int green = (y1192 - 833 * v - 400 * u);  
		                        int blue = (y1192 + 2066 * u);
		
		                        if (red<0) red=0; if (red>MAX_COLOR_VAL) red=MAX_COLOR_VAL;
		                        if (green<0) green=0; if (green>MAX_COLOR_VAL) green=MAX_COLOR_VAL;
		                        if (blue<0) blue=0; if (blue>MAX_COLOR_VAL) blue=MAX_COLOR_VAL;
		
		                        totalRed += red;
		                        totalGreen += green;
		                        totalBlue += blue;
		                    }
		                
		                }
		                int averageBright = totalBright / samples;
		                result.asciiIndexes[asciiIndex] = (averageBright * result.pixelChars.length) / 256;
		                int averageRed = totalRed / samples;
		                int averageGreen = totalGreen / samples;
		                int averageBlue = totalBlue / samples;
		                
		                // for ANSI mode, force each RGB component to be either max or 0
		                if (result.colorType==Result.ColorType.ANSI_COLOR) {
		                    // Force highest color component to maximum (brightness is already handled by char).
		                	// Other components go to maximum if their ratio to the highest component is at least ANSI_COLOR_RATIO.
		                    int maxRG = (averageRed > averageGreen) ? averageRed : averageGreen;
		                    int maxColor = (averageBlue > maxRG) ? averageBlue : maxRG;
		                    if (maxColor > 0) {
		                        int threshold = (int)(maxColor * Result.ANSI_COLOR_RATIO);
		                        averageRed = (averageRed >= threshold) ? MAX_COLOR_VAL : 0;
		                        averageGreen = (averageGreen >= threshold) ? MAX_COLOR_VAL : 0;
		                        averageBlue = (averageBlue >= threshold) ? MAX_COLOR_VAL : 0;
		                    }
		                }
		                result.asciiColors[asciiIndex] = (0xff000000) | ((averageRed << 6) & 0xff0000) |
		                                                ((averageGreen >> 2) & 0xff00) | ((averageBlue >> 10));
		                ++asciiIndex;
		            }
		        }
			}
		}else{
			if (nativeCodeAvailable) {
                getAsciiValuesBWNative(data, imageWidth, imageHeight, result.rows, result.columns, 
                        result.pixelChars.length, result.asciiIndexes, startRow, endRow);
            }else{
	        	int asciiIndex = startRow * result.columns;
	            for(int r=startRow; r<endRow; r++) {
	                // compute grid of data pixels whose brightness to average
	                int ymin = imageHeight * r / result.rows;
	                int ymax = imageHeight * (r+1) / result.rows;
	                for(int c=0; c<result.columns; c++) {
	                    int xmin = imageWidth * c / result.columns;
	                    int xmax = imageWidth * (c+1) / result.columns;
	                    
	                    int totalBright = 0;
	                    int samples = 0;
	                    for(int y=ymin; y<ymax; y++) {
	                        int rowoffset = imageWidth * y;
	                        for(int x=xmin; x<xmax; x++) {
	                            samples++;
	                            totalBright += (0xff & data[rowoffset+x]);
	                        }
	                    }
	                    int averageBright = totalBright / samples;
	                    result.asciiIndexes[asciiIndex++] = (averageBright * result.pixelChars.length) / 256;
	                }
	            }
            }
        }
	}
	
}