// Copyright (C) 2012 Brian Nenninger

package com.dozingcatsoftware.asciicam;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import asciiUtils.AsciiPackage;
import asciiUtils.Result;
import asciiUtils.Result.ColorType;

import com.dozingcatsoftware.util.SocketThread;

/** 
 * This class converts pixel data received from the camera into ASCII characters using brightness
 * and color information. 
 * Note: place on server
 */
public class AsciiConverter implements Serializable{
    
    static final boolean DEBUG = false;

	static ObjectOutputStream toServer;
	static ObjectInputStream fromServer;
    
    /** Image processing can be broken up into multiple workers, with each worker computing a portion of
     * the image rows. This allows using all CPU cores on multicore devices.
     */
    
    
//    class Worker implements Callable {
//        // identifies which segment this worker will compute
//        int totalSegments;
//        int segmentNumber;
//        // image parameters set for every frame in setValues
//        byte[] data;
//        int imageWidth;
//        int imageHeight;
//        int asciiRows;
//        int asciiColumns;
//        String[] pixelChars;
//        ColorType colorType;
//        Result result;
//        
//        public Worker(int totalSegments, int segmentNumber) {
//            this.totalSegments = totalSegments;
//            this.segmentNumber = segmentNumber;
//        }
//        
//        public void setValues(byte[] data, int imageWidth, int imageHeight, int asciiRows, int asciiColumns, 
//                String[] pixelChars, ColorType colorType, Result result) {
//            this.data = data;
//            this.imageWidth = imageWidth;
//            this.imageHeight = imageHeight;
//            this.asciiRows = asciiRows;
//            this.asciiColumns = asciiColumns;
//            this.pixelChars = pixelChars;
//            this.colorType = colorType;
//            this.result = result;
//        }
//        
//        @Override public Object call() {
//            // returns time in nanoseconds to execute
//            long t1 = System.nanoTime();
//            int startRow = asciiRows * segmentNumber / totalSegments;
//            int endRow = asciiRows * (segmentNumber + 1) / totalSegments;
//            result = computeResultForRows(data, imageWidth, imageHeight, asciiRows, asciiColumns, 
//                    colorType, pixelChars, result, startRow, endRow);
//            Log.d("AsciiConverter", " (compute result for rows) after: " + result.isChanged);
//            return System.nanoTime() - t1;
//        }
//    }
//    
//    
    private class CreateSocketTask extends AsyncTask<Void, Void, Socket> {
        @Override
        protected Socket doInBackground(Void... params) {
        	SocketThread socketThread = new SocketThread(AsciiCamActivity.PORT_NAME,AsciiCamActivity.PORT_NUM);
            socketThread.run();
            try {
    			socketThread.join();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
           return socketThread.getSocket();
        }
    }
//    
//    ExecutorService threadPool;
//    List threadWorkers;
//    
//    public void initThreadPool(int numThreads) {
//        destroyThreadPool();
//        if (numThreads<=0) numThreads = Runtime.getRuntime().availableProcessors();	//creates threads based on available processors
//        threadPool = Executors.newFixedThreadPool(numThreads);
//        threadWorkers = new ArrayList();
//        for(int i=0; i<numThreads; i++) {
//            threadWorkers.add(new Worker(numThreads, i));
//        }
//    }
//    
//    public void destroyThreadPool() {
//        if (threadPool!=null) {
//            threadPool.shutdown();
//            threadPool = null;
//        }
//    }
    
    public static String[] toPixelCharArray(String str) {
        if (str==null || str.length()==0) return null;
        String[] charArray = new String[str.length()];
        for(int i=0; i<str.length(); i++) {
            charArray[i] = str.substring(i, i+1);
        }
        return charArray;
    }
    
//    public Result resultForCameraData(byte[] data, int imageWidth, int imageHeight,
//            int asciiRows, int asciiCols, ColorType colorType, String pixelCharString) {
//        Result result = new Result();
//        computeResultForCameraData(data, imageWidth, imageHeight, asciiRows, asciiCols, colorType, pixelCharString, result);
//        return result;
//    }
    
//    public Result computeResultForCameraData(byte[] data, int imageWidth, int imageHeight,
//            int asciiRows, int asciiCols, ColorType colorType, String pixelCharString, Result result) {
//        long t1 = System.nanoTime();
//        result.debugInfo = null;
//        if (threadPool==null) {
//            initThreadPool(0);
//        }
//        for(Object worker : threadWorkers) {
//            ((Worker)worker).setValues(data, imageWidth, imageHeight, asciiRows, asciiCols, toPixelCharArray(pixelCharString), colorType, result);
//        }
//        try {
//        	// invoke call() method of all workers and wait for them to finish
//            List threadTimes = threadPool.invokeAll(threadWorkers);
//            Log.d("AsciiConverter", " (computeResultForCameraData1) after: " + result.isChanged);
//            if (DEBUG) {
//                long t2 = System.nanoTime();
//                StringBuilder builder = new StringBuilder();
//                for(int i=0; i<threadTimes.size(); i++) {
//                    try {
//                        long threadNanos = (Long)((FutureTask)threadTimes.get(i)).get();
//                        builder.append(String.format("Thread %d time: %d ms", i+1, threadNanos/1000000)).append("\n");
//                    }
//                    catch(ExecutionException ex) {}
//                }
//                builder.append(String.format("Total time: %d ms", (t2-t1) / 1000000));
//                result.debugInfo = builder.toString();
//            }
//        }
//        catch(InterruptedException ignored) {}
//        Log.d("AsciiConverter", " (computeResultForCameraData2) after: " + result.isChanged);
//        return result;
//    }
    
    // For ANSI mode, if a color component (red/green/blue) is at least this fraction of the maximum
    // component, turn it on. {red=200, green=180, blue=160} would become yellow: green ratio is
    // 0.9 so it's enabled, blue is 0.8 so it isn't.
    final float ANSI_COLOR_RATIO = 7.0f/8;

    /** Main computation method. Takes camera input data, number of ASCII rows and columns to convert to, and the ASCII
     * characters to use ordered by brightness. For each ASCII character in the output, determines the corresponding
     * rectangle of pixels in the input image and computes the average brightness and RGB components if using color. 
     */
    public Result computeResultForRows(byte[] data, int imageWidth, int imageHeight,
            int asciiRows, int asciiCols, asciiUtils.Result.ColorType colorType, String[] pixelChars, asciiUtils.Result result,
            int startRow, int endRow) {
    	
        CreateSocketTask createSocket = new CreateSocketTask();
        Socket sock = null;
		try {
			sock = createSocket.execute().get();
		} catch (InterruptedException e2) {
			e2.printStackTrace();
		} catch (ExecutionException e2) {
			e2.printStackTrace();
		}
        try {
			createSocket.get();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			e1.printStackTrace();
		}
        if(sock != null){
	        result.rows = asciiRows;
	        result.columns = asciiCols;
	        result.colorType = colorType;
	        if (pixelChars==null) pixelChars = colorType.getDefaultPixelChars();
	        result.pixelChars = pixelChars;
	        
	        if (result.asciiIndexes==null || result.asciiIndexes.length!=asciiRows*asciiCols) {
	            result.asciiIndexes = new int[asciiRows * asciiCols];
	        }
	    	
	        if (result.asciiColors==null || result.asciiIndexes.length!=asciiRows*asciiCols) {
	            result.asciiColors = new int[asciiRows * asciiCols];
	        }
	      
	            Log.v("SOCKET", "Socket started");
	        AsciiPackage before = new AsciiPackage(result, data, imageWidth, imageHeight,
	        		startRow, endRow);	        
	        ClientThread toSer = new ClientThread(before, sock);
	        toSer.run();
	        try{
	        	toSer.join();  
	        	Log.v("SOCKET", "Socket started3");
	        }catch (InterruptedException e){
	        	e.printStackTrace();
	        }
	        result = toSer.after.result;
	        data = toSer.after.data;
	        imageWidth = toSer.after.imageWidth;
	        imageHeight = toSer.after.imageHeight;
	        asciiRows = toSer.after.result.rows;
	        asciiCols = toSer.after.result.columns;
	        colorType = toSer.after.result.colorType;
	        pixelChars = toSer.after.result.pixelChars;
	        result.asciiColors = toSer.after.result.asciiColors;
	        result.asciiIndexes = toSer.after.result.asciiIndexes;
        }
        return result;
    }
    
    /** Builds an ASCII image from an existing bitmap. Used to convert existing pictures; not
     * native or threaded because speed is less important.
     */
    public Result computeResultForBitmap(Bitmap bitmap, 
            int asciiRows, int asciiCols, ColorType colorType, String pixelCharString) {
        Result result = new Result();
        result.rows = asciiRows;
        result.columns = asciiCols;
        result.colorType = colorType;
        result.asciiColors = new int[asciiRows*asciiCols];
        result.asciiIndexes = new int[asciiRows*asciiCols];
        result.pixelChars = (pixelCharString!=null) ? 
                toPixelCharArray(pixelCharString) : colorType.getDefaultPixelChars();

        // TODO: read bitmap pixels into array for faster processing
        int[] pixels = null;
        int asciiIndex = 0;
        for(int r=0; r<asciiRows; r++) {
            // compute grid of data pixels whose brightness and colors to average
            int ymin = bitmap.getHeight() * r / asciiRows;
            int ymax = bitmap.getHeight() * (r+1) / asciiRows;
            // read all pixels for this row of characters
            if (pixels==null) pixels = new int[(ymax-ymin+1) * bitmap.getWidth()];
            bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, ymin, bitmap.getWidth(), ymax-ymin);
            for(int c=0; c<asciiCols; c++) {
                int xmin = bitmap.getWidth() * c / asciiCols;
                int xmax = bitmap.getWidth() * (c+1) / asciiCols;
                
                int totalBright = 0;
                int totalRed=0, totalGreen=0, totalBlue=0;
                int samples = 0;
           
                for(int y=ymin; y<ymax; y++) {
                    int poffset = (y-ymin)*bitmap.getWidth() + xmin;
                    for(int x=xmin; x<xmax; x++) {
                        samples++;
                        
                        int color = pixels[poffset++];
                        //int color = bitmap.getPixel(x, y);
                        int red = (color >> 16) & 0xff;
                        int green = (color >> 8) & 0xff;
                        int blue = color & 0xff;
                        // Y = 0.299R + 0.587G + 0.114B
                        totalBright += (int)(0.299*red + 0.587*green + 0.114*blue);
                        totalRed += red;
                        totalGreen += green;
                        totalBlue += blue;            
                    }
                }
                int averageBright = totalBright / samples;
                result.asciiIndexes[asciiIndex] = (averageBright * result.pixelChars.length) / 256;
                if (asciiIndex%50==0) {
                    android.util.Log.i("color", String.format("%d %d %d %d", 
                            averageBright, samples, result.pixelChars.length, result.asciiIndexes[asciiIndex]));
                }
                
                if (colorType!=ColorType.NONE) {
                    int averageRed = totalRed / samples;
                    int averageGreen = totalGreen / samples;
                    int averageBlue = totalBlue / samples;
                    // for ANSI mode, force each RGB component to be either max or 0
                    if (colorType==ColorType.ANSI_COLOR) {
                        // Force highest color component to maximum (brightness is already handled by char).
                        // Other components go to maximum if their ratio to the highest component is at least ANSI_COLOR_RATIO.
                        int maxRG = (averageRed > averageGreen) ? averageRed : averageGreen;
                        int maxColor = (averageBlue > maxRG) ? averageBlue : maxRG;
                        if (maxColor > 0) {
                            int threshold = (int)(maxColor * ANSI_COLOR_RATIO);
                            averageRed = (averageRed >= threshold) ? 255 : 0;
                            averageGreen = (averageGreen >= threshold) ? 255 : 0;
                            averageBlue = (averageBlue >= threshold) ? 255 : 0;
                        }
                    }
                    result.asciiColors[asciiIndex] = (0xff000000) | (averageRed << 16) |
                                                    (averageGreen << 8) | averageBlue;
                }
                ++asciiIndex;
            }
        }
        return result;
    }
    
    

}
