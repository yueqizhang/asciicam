package com.dozingcatsoftware.asciicam;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.net.Socket;

import android.util.Log;
import asciiUtils.AsciiPackage;

public class ClientThread extends Thread{
	AsciiPackage before = null;
	AsciiPackage after;
	Socket soc;
	ObjectOutputStream toServer;
	ObjectInputStream fromServer; 
	
	public ClientThread(){
		before = null;
	}
	
	public ClientThread(AsciiPackage b, Socket s){
		if(b instanceof AsciiPackage){
			Log.v("ClientThread", "constructor" + b.result.asciiColors[10]);
			before = b;
		}
		after = null;
		soc = s;

		try {
			toServer = new ObjectOutputStream((soc.getOutputStream()));
			fromServer = new ObjectInputStream((soc.getInputStream()));
		} catch (IOException e) {
			System.err.println("Error!");
			e.printStackTrace();
		}	
	}
	public void run(){	
				try {
					if(before != null){
						Log.v("ClientThread", "" + before.result.asciiColors[10]);
						toServer.writeObject(before);
						after = (AsciiPackage)fromServer.readObject();
						toServer.flush();
					}else{
						Log.v("CLIENT", "before is null");
					}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}finally{
				try {
					toServer.close();
					fromServer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	}
}
