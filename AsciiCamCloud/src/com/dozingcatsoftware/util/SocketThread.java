package com.dozingcatsoftware.util;

import java.io.IOException;
import java.net.Socket;

public class SocketThread extends Thread{
	private String portName;
	private int portNum;
	private Socket soc;
	
	public SocketThread(String name, int num){
		portName = name;
		portNum = num;
	}
	
	
	public void run(){
		try{
			soc = new Socket(portName, portNum);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Socket getSocket(){
		return soc;
	}
	
}
